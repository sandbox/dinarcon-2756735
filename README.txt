CKEditor Clipboard
==================

Description
===========
This plugin handles cut/copy/paste inside of the editor, processed the clipboard
content on pasting, makes it better fit in the editor context, or even stripped
it down into plain text. It opens a dialog when the pasting meets browser
security constraints.

Installation
============

This module requires the core CKEditor module.

1. Download the plugin from http://ckeditor.com/addon/clipboard compatible with
   CKEditor versions 4.5 or higher.
2. Place the plugin in the root libraries folder (/libraries).
3. Enable CKEditor Balloon Panel module in the Drupal admin.

Uninstallation
==============
1. Uninstall the module from 'Administer >> Modules'.

MAINTAINERS
===========
Mauricio Dinarte - https://www.drupal.org/u/dinarcon

Credits
=======
Initial development and maintenance by Agaric.
